
function addToCart(ctx) {

    let {context, data} = ctx;
    let {lalapanType, quantity} = context;

    if (lalapanType && quantity) {
        if (data.cart === undefined) data.cart = [];

        data.cart.push({lalapanType, quantity});
    }

    return ctx;
}